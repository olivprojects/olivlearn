/** @type {import('@docusaurus/types').DocusaurusConfig} */
const math = require('remark-math');
const katex = require('rehype-katex');
module.exports = {
  title: 'Block Course',
  tagline: 'Bockchain para Desenvolvedores',
  url: 'https://olivprojects.gitlab.io/olivlearn/',
  baseUrl: '/olivlearn/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/logo.png',
  organizationName: 'Lunes Fundation', // Usually your GitHub org/user name.
  projectName: 'olivmath', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Home',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'onboard',
          position: 'left',
          label: 'Cursos',
        },
        { to: '/blog', label: 'Posts', position: 'left' },
        { to: '/articles', label: 'Artigos', position: 'left' },
        { to: '/about', label: 'Sobre', position: 'right' },
        {
          href: 'https://gitlab.com/olivprojects/olivlearn',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Blockchains',
          items: [
            {
              label: 'Bitcoin',
              to: 'https://bitcoin.org/pt_BR/',
            },
            {
              label: 'Lunes',
              to: 'https://lunes.io/',
            },
            {
              label: 'Ethereum',
              to: 'https://ethereum.org/',
            },
            {
              label: 'Cartesi',
              to: 'https://cartesi.io',
            },
            {
              label: 'Waves',
              to: 'https://waves.tech',
            },
            {
              label: 'IOTA',
              to: 'https://iota.org/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Discord',
              href: 'https://discord.gg/uYh5HvBX',
            },
            {
              label: 'Linkedin',
              href: 'https://linkedin.com/in/olivmath',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/olivmath_',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Livros',
              to: '/books',
            },
            {
              label: 'White Papers',
              href: '/whitepapers/',
            },
          ],
        }
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Block Course, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          remarkPlugins: [math],
          rehypePlugins: [katex],
          editUrl:
            'https://gitlab.com/olivprojects/olivlearn/-/tree/main/website',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/olivprojects/olivlearn/-/tree/main/website',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  // stylesheets: [
  //   {
  //     href: "https://cdn.jsdelivr.net/npm/katex@0.13.11/dist/katex.min.css",
  //     integrity: "sha384-Um5gpz1odJg5Z4HAmzPtgZKdTBHZdw8S29IecapCSB31ligYPhHQZMIlWLYQGVoc",
  //     crossorigin: "anonymous",
  //   },
  // ],
};
