import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Simples não Simplista',
    // Svg: require('../../static/img/<IMAGEM.SVG>').default
    description: (
      <>
        Cursos formatados com o cuidado que qualquer autoditata gostaria de encotrar.
        Sim você achou uma mina de ouro!
      </>
    ),
  },
  {
    title: 'Variedade Interelacionada',
    // Svg: require('../../static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Os Cursos são sobre diferentes temas que estão super relacionados entre si.
        Mais que uma coleção, é minha visão sobre tercnologia.
      </>
    ),
  },
  {
    title: 'Não Existe Almoço Grátis!',
    // Svg: require('../../static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Me pague um café se conseguiu se divertir!
      </>
    ),
  },
];

function Feature({title, description}) {
  return (
    <div className={clsx('col col--4')}>
      {/* <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div> */}
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
