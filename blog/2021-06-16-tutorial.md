---
slug: tutorial #Nome no arquivo
title: Tutorial de Post #Titulo do arquivo
author: Lucas Oliveira #Nome do Autor
author_title: Matemático | Dev. Blockchain @LunesPlatform #Cargo do Autor e Empresa
author_url: https://gitlab.com/olivmath_ #GitHub do Autor
author_image_url: https://avatars.githubusercontent.com/u/50037567?v=4 #Foto do Autor pode ser obtida api.github.com/users/USER-DO-AUTOR
tags: [Init, Lorem, Ipsum] # Tags desse post
---

## Aqui você pode escrever seu post!


### What is Lorem Ipsum?

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
