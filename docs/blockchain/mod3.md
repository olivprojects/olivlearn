---
sidebar_position: 5
---


# Tipos de Usuários

Existem diferentes tipos de usuários que fazem uso da rede Bitcoin e esses tipos se categorizam de acordo com o conjunto de entidades fundamentais que eles contém, as entidades fundamentais são:

- Full-Node: *Tem uma cópia completa da blockchain*
- Wallet: *Gerenciador de chaves públicas e privadas*
- Networking-Routing-Node: *Descobre e mantém conexão com outros pares*
- Miner: *Validador de blocos para a rede*

Baseado nisso cada combinação de entidade pode gerar um novo tipo de usuário ou extenção da rede Bitcoin como por:

**Bitcoin Core**

- Miner 
- Wallet
- Full-Node
- Networking-Routing-Node.

**Full Block Chain Node**

- Full-Node
- Networking-Routing-Node

**Solo Miner**

- Full-Node
- Networking-Routing-Node
- Miner

**Lightwight Wallet (SPV)**

- Wallet
- Networking-Routing-Node

**Pool Protocol Serves**

- Pool Server
- Stratum Server

**Mining Nodes**

- Miner
- Pool Server or Stratum Server

**Lightweight Stratum Wallet**

- Wallet
- Stratum Node

## Carteira

**High Level Overview Wallet**

As Wallets tem a função de gerir nossas chaves e armazenar nossas transações, elas **não armazenam nosso bitcoins**. Se dividem em *hot-wallets* ou *cold-wallets*, isso tem haver com fazer esse armazenamento conectado a internet *hot-wallet*, ou desconetado da internet *cold-wallet*. É possivel obter *hot-wallets* baixando um simples app no seu smartphone, ou no caso das *cold-wallets* é possivel que o software seja executado em um gadget que mantém um ambiente isolado sem conexão com a intenet que é chamado de *hard-wallet*. Também existem as *paper-wallets* que armazenam suas chaves pública e privada em um papel. Por fim existe uma altenativa mais segura que as anteriores a *brain-wallets*. Nesse módelo é necessário escolher um **código mnemônico** ou seja **12 palavras aleatórias** que depois de um processo *hash* se transformam na sua chave privada, sendo armazenada apenas no sua mente, para uma maior segurança as palavras não devem conter ligações ou um nivel alto de entropia, e devem ser passar por um processo de *hash* e um  número de vezes escolhido por você, por exemplo: usar SHA512 por 30 vezes.

**Low Level Overview Wallet**

**Lightwight Wallet (SPV)**
 isso é especialmente útil para uma wallet pois da a possibilidade rodar esse software em dispositivos com restrição de espaço e processamento, como smartphones, tablets e dispositivos embarcados.
**Simplified Payment Verification (SPN)** ou **Verificação Simplificada de Pagamento** é um método para verificar se uma transação particular está incluída em um bloco sem baixar a cadeia de blocos(blockchain) inteira. Isso é feito buscando a **BlockId** depois buscamos o **TxId** dentro da **merkle tree** do bloco e verificamos se a transação é valida. Para adicionar mais segurança ao processo podemos:

- conectar a vários nodes diferentes
- espera a trasação ter muitas confirmações (deep in chain)
 
**Boas Praticas**

- **Código Minemonico de Palavras**: Ao invés de gerar a chave usando números aleatórios, são esccolhidas 12 palavras aleatórias que depois de passar numa função hash formarão a chave privada. Assim é possivel decorar a chave privada, ex:
    - **private key**: palavras = ["casa", "carro", "pato" ...]
    - **hash(palavras)** -> ab24bacdf5623986feca17217

- **Multiassinaturas**: Podem ser muito uteis para prevenir perdas de chaves privadas, ex:
    - **multsig(2,3)**: Temos 3 chaves privadas das quais precisamos apenas de duas para assinar transações, nesse caso se uma chave fosse perdida poderiamos recuperar os fundos com as 2 restantes.

    - **multsig(2,3)**: Numa Exchange o cliente fica com 2 chaves, caso ele perca uma a Exchange pode oferecer uma chave para que o cliente recupere os fundos.

-  **Geração de Chaves**:
    - **1TX1KEY**: Para que se mantenha a anonimidade troque suas chaves a cada tran sação.

    - **MultFaill**: Para que não haja um ponto único de falha, distribua seus fundos em várias chaves diferentes usando **multsig(m,n)**.

    - **Carteiras de Hierarquia Deterministica**: Cria-se uma única chave privada ou **seed** e cada nova carteira gerada será o hash da seed mais indice da nova chave, ex:
        - seed: ab342cd09875afe76512b
        - key1: hash( ab342cd09875afe76512b + 1 )
        - key2: hash( ab342cd09875afe76512b + 2 )
    

## Mineração

A mineração é o processo pelo qual a rede mantém a blockchain segura e funcionando. Os nós que fazem a mineração são repsonsaveis por validar as transações, agrupá-las em blocos e progar os novos blocos pela rede. Como forma de manter esse esforço sustentavel é pago aos nós que o fazem um taxa de cada transação incluida no novo bloco como também um prêmio que é chamado de transação coinbase que é resultado do Proo-of-Work ou prova de trabalho que é um difícil problema matemático com base em um algoritmo de hash criptográfico. Essa competição para resolver o algoritmo de Prova de Trabalho para ganhar a recompensa e o direito de registrar transações na cadeia de blocos é a base para o modelo de segurança do bitcoin.

Esse prêmio é decresente e cai pela metade a cada 210000 blocos, isso faz com que a quantidade de moedas na rede atinja um limite de 20.99999998M ou aproximadamente 21M, isso é previsto para acontecer no ano de 2140. depois disso o prêmio de mineração vai a 0, abrindo mais espaço no bloco para novas transações de modo a transferir os incentivos para as taxas de transações.

**Etapas de Mineração**

0. Download da Blockchain completa, isso é feito a penas uma vez.
1. Verificar as Transações:
    - Guardar todas as transações recebidas num buffer chamdo "mempool".
    - Selecionar as Transações com maior taxa.
    - Verificar se as transações selecionadas atendem a uma lista abrangente de certos critérios de segurança.
2. Criar um bloco:
    - Gerar o cabeçalho com as informações necessárias.
    - Construir a Árvore de Merkle com a lista das transações válidas.
3. Prova de Trabalho:
    - Encontrar o Nonce com o hash do bloco.
    - NOTA: é possivel fazer o hash da coinbase para aumentar número de hash possiveis, caso o hash do bloco não seja suficiente para encontrar a prova de trabalho menor do que o alvo.
4. Propagar o bloco "minerado" pela rede para ser adicionado a cadeia de blocos.
5. Isso seu bloco chegar a cadeia mais longa você recebe seu lucro(taxas mais coinbase).

## Governança


**Paradoxo da Mineração**

Quanto mais segura a rede se torna com aumento da difículdade de hash maior deve ser o poder computacional para continuar minerando a rede, isso acabar por incentivar pools ou picinas de mineração que dividem o trabalho de buscar um nonce válido e diminuindo o risco de passar decádas sem obter recompensar por "minerar" blocos. Isso centraliza a rede em pools deixando ela mais atraente a ataques.

![Descentralização - Anonimidade](/img/mod3/Anonimidade.png)

Para resolver o problema da centralização podemos modificar o Proof-fo-Work, mas o novo puzzle deve conter algumas caracteristicas:

- Fácil de Verificar
- Difículdade Ajustavél
- Computacionalmente Difícil
- Taxa de Resolução a Taxa de HashPower da Rede
- Livre de Progresso: O próximo não depende do anterior
- Gerado Pseudo-Aleatóriamente

Há algumas dessas mudanças implementadas em algumas moedas como [Dogcoin](https://dogecoin.com), [Dash](https://dash.org/pt-br) [Litcoin](http://litcoin.org/pt).
O protocolo sempre evolui e essas e outras mudanças podem acontecer e isso fica por responsabilidade do Bitcoin Core Team.

**Bitcoin Core Team**

No Bitcoin, as mudanças no protocolo vêm na forma de Propostas de melhoria do Bitcoin ou BIPs(*Bitcoin Improvement Proposals*).
Podem ser mudanças no protocolo de rede, validação de bloco ou transação, ou qualquer coisa afetando a interoperabilidade. É assim que a comunidade vota nas mudanças que deseja apoiar. Os mineiros votam nos BIPs incluindo uma referência a esse BIP no bloco que mineram. Existem três tipos de BIPS:

 - **Padrão:**
    - É uma atualização ou alteração real do protocolo. É assim que o ecossistema muda e em que os mineiros votam.
 - **Informativo:**
    - BIPs informativos são mais orientações sobre como as pessoas devem fazer as coisas no futuro, mas não muda o protocolo. Um exemplo de BIP informativo seria um BIP falando sobre como administrar um pool de mineração.

 - **Processo:**
    - São coisas específicas que devem ser feitas, assim como as melhores práticas. O primeiro BIP foi feito por Amit Taaki em 2011, e era na verdade um BIP para fazer BIPs.


Essas mudanças podem gerar mudanças suaves ou rigidas que são conhecidas como Soft Fork e Hard Fork respectivamente.


**Hard Fork e Soft Fork**

*Hard Fork* 

O Hard Fork não é compatível com versões anteriores e isso resulta em um novo protocolo que faz coisas não permitidas no protocolo antigo. Um exemplo é o Bitcoin Cash, que aumentou o tamanho do bloco de 1 MB para 8 MB. Os nós no protocolo Bitcoin antigo e legado recusarão os novos blocos de 8 MB, levando a uma bifurcação permanente.

*Soft Fork*

Já no Soft Fork, as regras do novo protocolo são apenas restringidas. Por exemplo, se um novo fork de Bitcoin chamado Bitcoin Dollars reduzisse o tamanho do bloco
a 0.5 Mb(megabyte), então tudo o que é válido no novo protocolo ainda é válido no protocolo antigo. No entanto, os blocos do protocolo antigo(Bitcoin) não são mais válidos no novo protocolo(Bitcoin Dollars), isso significa que um soft fork é compatível com versões anteriores, mas não compatível com versões posteriores.

## Segurança

Existem algumas vunerabilidades que podem ser exploradas na maioria dos protocolos blockchain, vamos a eles:

**Estratégias em Picinas(Pools)**

Tipos de recompensas pagas por pools:

- Propocional: Nesse tipo de Pool as recompensas são proporcionais a sua ação de entrergar hashs, são altas quando se está no inicio de um novo bloco, mas inversamente proporcional ao número de ações submetidas.
- Por ação: A recompensa é constante e não importa quantas ações foram submetidas.

*Salto de Piscina (POOL HOPPING)*

Porém trocar de pool no momento certo pode aumentar as recompensas totais. Por exemplo:
- Minerar em um pool *Proporcional* logo após um bloco ser encontrado (enquanto as recompensas são altas).
- Mudar para um pool de *Pagamento por Ação* uma vez que o pool *Proporcional* é menos lucrativo.

![Pools](/img/mod3/pool.png)

Portanto, os pools de *Recompensas Proporcionais* não são viável na prática. Pois mineiros honestos que permanecem leais a esse tipo de Pools são roubados.

Projetar um esquema de recompensa do pool de mineração com incentivos alinhados que não são
vulnerável ao *Salto de Piscina (POOL HOPPING)* permanece um problema aberto.

**Canibalismo em Pools**

Como os pools de *Pagamento por Ação* não podem verificar se os nós estão minerandos blocos válidos, esses nós podem se conectar a vários pools para enviar blocos inválidos com pouca de chance de serem detectados. Ex:

- Dado que um nó tem 30H/s(HashPower/segundo) em uma REDE BLOCKCHAIN com um total de 100H/s.
- Assumindo que a recompensa é de 1BTC/B(1BTC por bloco).
- Esse nó tem 30% do HR(HashRate) total da REDE BLOCKCHAIN.
- Logo sua recompensa por bloco será 30% * 1BTC/B = 0.3BTC/B.

Agora se o nó comprar mais hardware a ponto de ter agora 31H/s o que significa ter 31/101 = 30.69% HR da rede, por consequência **0.0069BTC a mais**. Mas se esse nó Canibalizar outras pools poderá ganhar mais. Ex:

- O nó distribui apenas 1H/s para não ser detectado, que não envia blocos válidos, em todas as outras pools.
- Supondo uma outra pool com 70H/s restante da REDE BLOCKCHAIN total e honestos com o mesmo 1BTC/B como recompensa.
- Se o nó adicionar 1H/s a pool anterior, a pool terá 71H/s, mas com apenas 70H/s efetivos.
- O nó agora vai ser 1/71 sendo que apenas 70H/s são honestos.
- Então 1/71 * 0.7BTC = 0.0098BTC

Ou seja se:

- **Honesto 0.3068BTC**
- **Desonesto 0.3098BTC**

**Equilibrium de Nash**

Podemos modelar o comportamento de diferentes Atores na rede Bitcoin para uma determinada situação usando uma análise de **Teoria dos Jogos**. Nossa primeira suposição é que os Atores agirão de maneira racional, onde a racionalidade é definida como a execução de ações que maximizam sua utilidade. Em nosso cenário, a utilidade é definida pelos ganhos monetários resultantes de uma ação Embora nem todo mundo no Bitcoin seja puramente motivado por ganho monetário, essa ainda é uma boa generalização.

Uma **Pura Estratégia de Equilíbrio de Nash** é o conjunto de ações que maximizam a utilidade de cada Ator, dadas as respostas de todos os outros Atores. Observe que a utilidade que cada Ator recebe depende das próprias decisões e das decisões de todos os outros Atores, da mesma forma que as recompensas que um minerador obtém no pool de mineração dependem da sua própria decisão de atacar ou cooperar e a decisão do resto da pool de atacar ou cooperar. Os Atores na rede Bitcoin ou em um pool de mineração convergem para atuar de acordo com um **Genuíno Equilíbrio de Nash** para um determinado cenário, supondo que todos se comportem de maneira racional.

Vamos dar uma olhada em um cenário simples. Suponha que haja apenas 2 pools de mineração na rede Bitcoin, Pool A e Pool B. Seus valores utilitários artificiais são mostrados abaixo. O valor numérico da utilidade é arbitrário em economia; não possui unidades associadas e é calculado a partir de uma função utilidade de outros parâmetros, definidos pelo economista. Estamos interessados ​​apenas no valor comparativo da utilidade de uma ação, seja ela maior ou menor do que outra; não estamos interessados ​​em seu valor absoluto. Neste exemplo, as respectivas utilidades são derivadas dos ganhos monetários que cada Ator receberia de determinados cenários de ataque ou cooperação com o outro.

**Tabela de Valores Utilitários**

![Tabela de Valores Utilitários](/img/mod3/nash0.png)

Os valores numéricos específicos para os valores utilitários neste exemplo foram escolhidos arbitrariamente para refletir um cenário onde os Atores são incentivados a agir desonestamente, mas se saem pior quando ambos os Atores são desonestos do que quando ambos são honestos. Vamos considerar a perspectiva do Ator A e B agirem honestamente.


**Valores Utilitários após a Primeira mudança de Estado**

![Tabela de Valores Utilitários](/img/mod3/nash1.png)

Dado que o Ator B age honestamente, o Ator A ganha mais utilidade por agir desonestamente, uma vez que uma utilidade de agir desonestamente é 3 > 2.


**Valores Utilitários após a Segunda mudança de Estado**

![Tabela de Valores Utilitários](/img/mod3/nash2.png)

Dado que o Ator A age desonestamente, o Ator B prefere também agir desonestamente, uma vez que uma utilidade de 1 > 0. Porém os valores utilitários dos Atores A e B são menores por agirem de forma desonesta.


**Valores Utilitários após uma Terceira mudança de Estado**

![Tabela de Valores Utilitários](/img/mod3/nash3.png)


Podemos ver que o cenário em que ambos os Atores optam por agir desonestamente é o **Equilíbrio de Nash**, uma vez que nesta posição, ambos os Atores estão maximizando suas recompensas, dadas as ações do outro Ator. Portanto, apesar de receberem retornos mais elevados se ambos agirem honestamente, os Atores A e B agirão desonestamente.

**Gasto Duplo (DOUBLE SPEND)**

Uma estratégia de atacar a rede com *gasto duplo* é por meio do *race attack* ou ataque de corrida. Esse ataque funciona assim:

- Supondo uma compra de um artigo qualquer por um valor arbitrário. 
- Supondo também que a vítima é ingênua e apenas verifica se a transação foi efetua e **imediatamente** envia o artigo.
- O atacante deve fazer a transação e **imediatamente** e enviar o mesmo **UTXO** pra outros endereços de sua posse com uma **taxa maior** que a transação falsa. 
- Isso gera incentivos para que os mineradores não adicionem a transação desonesta em um bloco.

Uma maneira de evitar isso é esperar por **confirmações**. Confirmação é o número de blocos construidos após o bloco em que a transação foi minerada. Mas ainda há uma maneira de contornar o fator de confirmação da rede:

- Supondo que a vítima aceite enviar o artigo apenas de pois de **k** confimações da rede.
- O atacante enviar o UTXO para a vítima.
- Em paralelo o atacante minera um bloco contendo o mesmo UTXO porém em uma transação para ele memsmo.
- Agora basta o atacante minerar uma cadeia de tamanho **k + 1** para ela ser aceita na rede e invalidar o UTXO desonesto.

Vamos medir a change disso acontecer:

$$
    1 - \Sigma_{k=0}^{z} \frac{\lambda^k e^{-\lambda}}{k!} (1 - (\frac{q}{p})^{z - k})
$$

[fonte](/papers/whitepapers/bitcoin.pdf)
 

![Grafico_probabilidade](/img/mod3/prob.png)

O gráfico mostra que a medida que o Power Hashing aumenta, o risco de ataque a rede aumenta a ponto que um atacante tenha 50% ou mais do Poder de Mineração da rede, essa rede será 100% insegura. Há também um atacar que pode ser prática por grandes organizações que é chamado de **Goldfinger Ataque** inspirado no filme do 007. Nesse ataque o objetivo é apostar na queda do Bitcoin e fazer um *short* na moeda.

Segue a implementação em [Python](https://python.org):

```python
from math import e, factorial

def attack_success_probability(hash_rate: float, confimation: int) -> float:
    probability: float = 1 - hash_rate
    lamb: float = confimation * (hash_rate / probability)

    return 1 - sum(
        (lamb ** k) * e ** (-1 * lamb) / factorial(k) *
        (1 - (hash_rate / probability) ** (confimation - k))
        for k in range(0, confimation + 1)
    )


fun = attack_success_probability

hash_rates: list = [ 0.1, 0.2, 0.3, 0.4, 0.5 ]
confirmations: list = [ i for i in range(6) ]


r: dict = {}

for i in hash_rates:
    r[i] = []
    for j in confirmations:
        x = fun(i, j)
        r[i].append(x)


for i in r.keys():
    print(f"Hash Rate: {i}")
    confirmation: int = 0
    for j in r[i]:
        print(f"{confirmation}, {j}")
        confirmation += 1

```



**Censura**

Ataques de censura podem incluir censura de transações, nesse tipo de ataque a vítima é impossibilitada de ter sua transação inserida em um bloco pois o atacante detem 51% do Poder de Mineração da rede ou subornou esse equivalente de mineradores.

Fork punitivo é outro ataque de censura consequencia do anterior. Aqui os mineradores são descentivados a aceitar as transsações da vítima pois seus blocos minerados não entraram na cadeia mais longa pois, o atacante tem 51% do Hash Rate da rede de forma que ele sempre consegue minerar a cadeia mais longa

Caso o atacante tenho menos que 51% do Poder Total de Mineração da rede ele pode usar outra estratégia de censura. Esse ataque se chama **Feather Forking** ele funciona assim:

- Supondo que o atacante tem uma propoção **q** do poder total da rede, seja: 0 < q < 1.
- Supondo que o atacante tenha que minerar uma quantidade **k = 1** bloco a mais do que os outros mineradores com seu Hash Rate, isso da a ele uma chance de $$k^2$$ já que ele tem que competir com cada confirmação para deixar o bloco da vítima fora da cadeia mais longa.
- Sendo assim se seu Hash Rate for 0.2 então sua chance de deixar o bloco da vítima orfão é de 4%.

4% pode parecer pouco mas outros mineradores sabem que seus blocos tem 4% de chance de ficar orfão, isso faz com que eles pensem duas vezes antes de incluir o bloco da vítima. Para assumir esse risco a mais eles deve receber algo a mais, dessa forma a vítima fica forçada a pagar uma taxa maior de transação para que elas sejam incluidas em um bloco.

Com apenas 20% de Hash Rate foi possivel censurar a vítima.

**Retençao de Blocos**

Nesse tipo de ataque o minerador ao minerar um bloco ele não envia-o para rede mas continua para minerar o próximo, nesse segundo bloco o Poder Total de Mineração do atacante é de 100% pois os outros mineradores ainda pensam estar minerando a cadeia mais longa. Após isso o atacante pode submeter seus 2 novos blocos e receber a recompensa por eles.

Caso o restante da rede alcance ele antes do segundo pode acontecer o seguinte:

- Se o atacante tiver 25% do Poder Total de Mineração da rede e conseguir propagar seu bloco por 50% dos nós na rede, ele obtém lucro.
- Caso o atacante tenha 33% do Poder Total de Mineração ele pode sempre perder a corrida que ainda assim é compensatório.

A explicação matemática para isso está [papers/blocks_withholds.pdf)

**Defesas**

Um dos tipos de defesas que foi sugerida ao ataque de **Retenção de Blocos** é uma novo bloco fictícil assinado a frente do bloco atual, isso possibilitaria fácil identificação do atacante quando ele publica-se sua cadeia mais longa. Todavia esse tipo de defesa é frágil a **ataques de Sybil**, pois o nó malisioso pode gerar várias assinaturas sem esforço. 

Outra ideia para a resolução é a **Punição de Fork** nela os blocos minerados em paralelo não recebem recompensa até que a cadeia mais longa seja lançada. Então o minerador recebe a recompensa pelo bloco novo e metade da recompensa do bloco anterior. Isso puni os fork feitos na cadeia de blocos fazendo com que o atacante não obtenha vantagem em minerar uma cadeia de forma oculta em paralelo. O ponto negativo é que isso também pune os mineradores honestos e cria um dificuldade enorme na implementação pois exige alterações nas taxas de transações dos blocos que "ficam para trás" e que premiam apenas metade da recompensa.

Existem outras estratégias e tentativas de dissuadir ataques de retenção de bloco como **Quebra de Laço Uniforme (Uniform Tie Breaking)** **Publicar ou Perecer (Publish or Perish)** bem como ataques melhor arquitetados como **Ataques de Sybil**, **Ataques de Eclipse (Eclipse Attacks)** e **Ataque de Mineração Teimosa (Stubborn Mining)** que levam em conta a latência e topologia da rede.

Além de ataques individuais, a própria arquitetura da rede Bitcoin contém vulnerabilidades. O protocolo Bitcoin é P2P, no qual as mensagens são enviadas por meio de um protocolo de "fofoca" (*gossip*), onde cada nó passa uma mensagem para seus nós conectados. Embora desejemos uma topologia uniforme, esse não é o caso. Os nós com hashrate mais alto, talvez por participarem de pools de mineração, podem ter mineradores conectados a eles diretamente em um protocolo leve ou um subgrafo secreto. Devido a essa topologia de grafo oculto, um usuário poderia potencialmente ocultar que possui mais de 50% do poder de hash da rede.

Além da topologia da rede, a latência da rede também representa um problema. Os nós que estão melhor conectados à rede veem as coisas com mais rapidez e podem enviar mensagens e bloqueios para a rede com mais rápidez. Isso leva a lucros desproporcionais por parte de alguns mineradores.


referências:

[Publish or Perish: A Backward-CompatibleDefense against Selfish Mining in papers/Publish_or_Perish.pdf)

[ZeroBlock: Timestamp-Free Prevention ofBlock-Withholding Attack in papers/ZeroBlock.pdf)

[Theoretical Bitcoin Attacks with less than Half of the Computational Power (papers/Half_of_the_Computational_Power.pdf)

[Majority is not Enough: Bitcoin Mining is papers/Majority_is_not_Enough.pdf)