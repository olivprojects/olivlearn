---
sidebar_position: 1
---

# Road Map

**Módulo I**
- [x] Introdução: Bitcoin
- [x] Identidade: Public/Private Keys
- [x] Transações: UTX0
- [x] Concenso: Prof-of-Work
- [x] Revisão

**Módulo II**
- [x] Hash Function
- [ ] Assinaturas: Public/Private Keys
- [x] Matemática: ECDSA
- [x] Transações: P2PKH/P2PSH
- [x] Blocos: Block Hearder
- [x] Merkle Root: Merkle Tree
- [x] Previous Hash: Blockchain
- [x] Nonce: Proof-of-Work

**Módulo III**
- [x] Tipos de Usuários
- [x] Carteiras
- [x] Mineração
- [x] Governança
- [x] Segurança

**Módulo IV**
- [x] Contratos Inteligentes
- [x] Introdução ao Ethereum
- [x] Máquina Virtual Ethereum
- [x] Apicaçẽs e Ecosistema 
