---
sidebar_position: 3
---

# Introdução: Bitcoin

**Dinheiro**

A primeira e mais amplamente usada moeda com as seguintes caracteristicas:
- completamente digital
- decentralizada
- criptografada
- construida comprincipios de ciência computacional

**Cultura**

- CypherPunk
- Econômia

**Blockchian**

É um registro público de informações em ordem cronológica. A *blockchain* é compartilhada entre todos os usuários da Rede. Ele é usado para  organizar, recuperar e armazenar informações e impedir duplo gastos.

**Bitcoin**

É um Protocolo que integra diferentes tecnologias afim de possibilitar um meio de transações  financeiras sem intermediários seguro.

**White Paper**

Artigo Cientifíco que apresenta, explica e diz qual a problema determinada pesquisa se empenhou em resolver.
[White paper do Bitcoin](https://bitcoin.org/files/bitcoin-paper/bitcoin_pt_br.pdf)

**Pontos Altos do Bitcoin:**

- [Evitar Gasto Duplo](https://bitcoin.org/pt_BR/vocabulario#gasto-duplo)
- [Criptografia SHA256](https://pt.wikipedia.org/wiki/SHA-2)
- [Consenso Bizantino](https://medium.com/@luistikebit/toler%C3%A2ncia-a-falhas-bizantinas-bft-7a3adea9f3d2)
- [Remover Intermediários](https://www.crie.ufrj.br/destaque/precisamos-falar-sobre-bitcoin/401)
- [Evitar ataque Sybil](https://pt.wikipedia.org/wiki/Ataque_Sybil) 

**Bitcoin vs Bancos**

Os **Bancos** são responsaveis por gerir as contas dos usuários através de identificaçao antes de tomar qualquer ação de modo que se possa *linkar* cada ação a alguém.

Mais importante que isso, **Bancos** fornecem confiança pois são administrados constantes regulamentações do Governo e por profissinais qualificados de primeira linha.

Já no **Bitcoin** os participantes são responsaveis e administradores de suas próprias contas, sem a necessitade de relacionar sua identidade real a conta para realizar transações.

Em vista disso transfere-se a confiança da entidade *Banco* para a rede e seu protocolo *Bitcoin* que tem a caracteistica de ser público e imutável.

## Identidade: Public/Private Keys

**Autentitificação**

Tem a função de permitir que apenas o dono dos fundos faça transações e ninguém mais.
Também serve como meio de rastrear e impedir que individuos maliciosos se comuniquem com a rede.

**Integridade**

Diz respeito a capacidade que o processo de autentifição tem de não ser replicado, um método de autentificar é por meio de *chaves publicas e privadas* como nos email em nossas casas:

```
{
    user: exemplo@mail.com,
    pwd*: senh****
}
```

No Bitcoin a [geraçao](https://www.bitaddress.org/) de chaves contém os seguintes passos:
1. geração de uma **chave privada (256bits)** de forma aleatória *(2²⁵⁶ => 0.0000000058%)*
2. através de uma curva elíptica calculamos a **chave pública(32bits)**
3. Usando a função SHA256 na chave publica obtemos nosso **endereço (160bits)**

Por fim nossas chaves terão a seguinte aparencia:

```
{
    privateKey: KwKkff5gGcNCa7oz7wNqyK6zSMCA9UHa2ANX8agWbMz4RnTu9pqi
    publicKey: 1NX7BjU1nV1SPHApiNcDj2D5FouQsv626q 
}
```
**Chaves Publicas e Privadas**

As **chaves publicas** são usadas para se **comunicar** com os outros participantes da rede.

As **chaves privadas** são usadas para **assinar** transações.

## Transações: UTX0

**Validando Transações**

As transações acontecem por meio de UTX0 (Saídas de Transação não Realizadas). Para que uma transação seja válida é necessário:
- Assinatura
- Fundos suficientes
- Fundos unicos para cada trasação

**Rastreio de Transações**

As transações são agrupadas em blocos e salvas no banco de dados de forma a referênciar o bloco anterior, construindo assim uma cadeia de blocos, isto é, uma cadeia de transações validas. Essa validação é feita por um algritmo de concenso chamado Prova-de-Trabalho (Proof-of-Work).

## Concenso: Prof-of-Work

O problema do consenso aparece quando tentamos atualizar o blockchain, para evitar o gasto duplo e ataques Sybil o mecânismo de consenso usado é o voto por meio do Proof-of-Work.


**Proof-of-Work**
Ao invez de vincular o poder de voto a identidade dos participantes, o que facilitaria o ataques do tipo Sybil, o voto é atrelado ao poder computacional (1-voto -> 1-cpu).

Dessa forma antes de enciar uma atualização, ou bloco de transações, para a rede o **Nó** deve resolver um problema matemático complexo e enviar a resposta junto  com o bloco.

## Resumo

**Identidade**
- Os nós da rede usam suas **chaves públicas** para receberem transações e suas **chaves privadas** para assina transações e controlar fundos.

**Transações**
- Os nós transferem a propriedade das **UTX0s**.
- Vocẽ possui a soma das transações que você tem controle e não um saldo em conta.

**Blockchain**
- Uma eestrutura de dados que armazena as transações em blocos e liga-os por meio de um **Hash**.
- É o modelo central da arquitetura do **Bitcoin**.

**Consenso**
- Não há como garantir que cada Nó vote apenas uma vez nem como restringir o número de identidades(chaves públicas) criadas, então a rede usa o poder computacional para restringir entidades maliciosas, em outras palavras, o **Proo-of-Work**.

## Apêndice
**Pseudônimo**
- Os participantes da rede não usam seu nomes reais para fazer transações na rede.

**Descentralização**
- Todos os nós tem uma cópia do histórico de transações

**Imutabilidade**
- Impossibilidade de alterar o histórico de transações sem quebrar a rede, causando um **[fork](https://pt.wikipedia.org/wiki/Bifurca%C3%A7%C3%A3o_(desenvolvimento_de_software))**

**Trustless**
- O resultado das caracteristicas acima leva a uma rede onde não há necessidade de confiança nos atores e sim no próprio **protocolo** da rede