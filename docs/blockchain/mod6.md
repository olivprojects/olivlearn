---
sidebar_position: 8
---

# Consenso de Nakamoto

Lideres são eleitos como em uma loteria onde a probabilidade de ganhar aumenta conforme mais recursos forem fornecidos. O vencedor pode criar a nova atualização do sistema. A rede aceita ou não a atualizaçao gerando o consenso. Alguns exemplos de consenso: 

## [Proof-of-Work](https://en.wikipedia.org/wiki/Proof_of_work) (PoW)
- Consome poder computacional.
- Resolução de partes de pre-imagens *hash-puzzle*.
- Previne ataques Sybil.

## [Proof-of-Activity](https://www.investopedia.com/terms/p/proof-activity-cryptocurrency.asp) (PoA)
- Híbrido de PoW e PoS.

## [Proof-of-Burn](https://academy.binance.com/pt/articles/proof-of-burn-explained) (PoB)
- Consome literalmente moedas nativas.
- Como o PoS mas não é possível recuperar as moedas usadas para gnahar o direito de atualizar a rede.

## [Proof-of-Space](https://en.wikipedia.org/wiki/Proof_of_space) (PoS)
- Consome espaço de armazenamento.
- Recopensa diretamente quem fornece armazenamento.

## [Proof-of-Elapsed-Time](https://www.investopedia.com/terms/p/proof-elapsed-time-cryptocurrency.asp) (PoET)
- Consome uma quantidade aleatória de tempo.
- Quem "esperar" por um tempo aleatório ganhja o poder de atualizar o sistema.

## [Proof-of-Authority](https://academy.binance.com/pt/articles/proof-of-authority-explained) (PoA)
- Consome reputação relacionada a uma identidade.
- Permisionada.

## [Proof-of-Stake](https://en.wikipedia.org/wiki/Proof_of_stake) (PoS)
- Consome moedas nativas.
- Validadores ao invéz de mineradores.
- Trava de moedas como participação.

| **Baseado em Cadeia** | **Baseado em BFT** |
|------------------------------|--------------------------|
| Um validador é escolhido aleatóriamente com propriedade proporcional a quantidade de moedas em *stake*. | Um validador é escolhido aleatóriamente com propriedade proporcional a quantidade de moedas em *stake*. |
| O validador escolhido **Cria** um novo bloco que tem as novas transações e o *hash* do bloco anterior e atualiza o estado da rede. | O validador escolhido **Propoe** um novo bloco que tem as novas transações e o *hash* do bloco anterior e atualiza o estado da rede. |
| O validador recebe a recompensa pelo bloco e pelas transações adicionadas na cadeia | Os outros validadores votam se eles acham o bloco válido ou não. |
|  | Se mais de 2/3 dos validadores aceitarem a rede entra em consenso e o bloco entra na cadeia. |
|  | O validador recebe a recompensa pelo bloco e pelas transações adicionadas na cadeia |
