---
sidebar_position: 7
---
import Mermaid from '@theme/Mermaid';

# Sistemas Distribuidos

## Confiança sem Confiança

Blockchain: Uma **rede** onde individuos de várias partes do mundo possam se conectar e concordar com uma verdade comum sem confiar uns nos outros, apenas no **protocolo** e na **matemática** por traz da rede. O Consenso da rede tenta criar um sistema confiável apartir de partes potencialmente não confiáveis.


## Origens

- Os estudos sobre consenso foram iniciados por companias de aviões onde era necessário um tipo de sistema tolerante a falhas e empresas que precisavam manter *backup* seguros.

- Artigo: "Time, Clocks and Ordenation of Events in Distribution Systems" - Leslie Lamport.

## Fundamentos

Sistema Distribuido pode ser categorizado por duas partes de uma forma geral:

- **Nós (node)**: Máquinas ou processos separados. A Separação pode ser física ou de componetização, ex:
    - Nodes de Blockchain (físico).
    - Núcleos em uma CPU (Componentização).

- **Canal de Mensagem (Mensageria)**: Objetivo de demonstrar o moviemnto de informações entre **Nós**.
<Mermaid chart={`
	graph LR;
		a((A)) --> b((B)) & f((F));
        c((C)) --> a((A)) & e((E));
        f((F)) --> g((G)) & e((E));
        b((B)) --> e((E)) & f((F));
        e((E)) --> g((G));
        h((H)) --> a((A)) & c((C));
`}/>

---
**Componentes *comuns* em Sistemas Distribuidos**

- Simultaneidade
- Sem Relógio Global
- Tolerante a Falhas de Compenentes Únicos


### Propriedade de um Sistema Distribuido

Uma ótica para entender os sistemas distribuidos são as ideias de [Leslie Lamport](https://pt.wikipedia.org/wiki/Leslie_Lamport)

| **Segurança**                | **Vivacidade**           |
|------------------------------|--------------------------|
| O que não pode acontecer     | O que deve acontecer     |

![img](/img/mod5/sec_life.png)

Como saber se o sistema atinge o objetivo? Algoritmo de consenso! Isto é "Dado uma entrada, os nós tem que concordar com uma saída".

**Algoritmo de Consenso**

- **Validade**: Qualquer valor decidido deve ser proposto por um processo, não pode ser arbitrário.
- **Acordo**: Todos os processos, sem falhas, devem concordar como mesmo valor.
- **Terminação**: Todos os nós, sem falhas, eventualmente decidem.

| **Segurança**                | **Vivacidade**           |
|------------------------------|--------------------------|
| O que não pode acontecer     | O que deve acontecer     |
| Validade & Acordo            | Acordo                   |
| Nós honestos nunca irão decidir por valores asrbitrários ou aleatórios. | Todos os nós eventualmente decidem sobre um valor |

## Teorema CAP

O **teorema CAP** de [Eric Brewer](https://en.wikipedia.org/wiki/Eric_Brewer_(scientist)) diz que um sistema pode apenas ter 2 das 3 características **C.A.P.** (Isso em cassos Extremos ou Absolutos, na prática esses princínpios são mais maleavéis):

![img](/img/mod5/cap.png)


- **Consistência**: Cada nó fornece o estado mais recente do seu sistema, se não tiver o estado mais recente não forece nenhuma resposta.
- **Disponibilidade**: Cada nó tem acesso a leitura e gravação constante de qualquer estado do seu sistema.
- **Tolerância a Partições**: *Partições são a incapacidade de dois nós se comunicarem na rede.* Deve haver tolerância a partições, a rede deve funcionar apesar de partições.

| **Segurança**                | **Vivacidade**           |
|------------------------------|--------------------------|
| O que não pode acontecer     | O que deve acontecer     |
| Consistência & Tolerâcia a Partições | Disponibilidade |
| Apesar das partições a rede não para de fornecer o seu estado mais recente, caso contrário não entrega resposta. | A rede deve estar disponível a gravação e leitura de valores contantemente |

## Tolerância a Falha Bizantina

A falha dos generais bizantinos consiste no seguinte problema: Um exército cerca uma cidade inimiga mas só pode obter sucesso se todos os generais atacarem ou recuarem simultaneamente, caso contrário irá ser derrotado. Os generais estão separados físicamente e podem usar mensageiros. Porém os mensageiros podem se perder ou corromper a mensagem, como também há um número de generais desconhecidos que foi subornado e irá atrapalhar a operação. Esses generais traidores são denominados **Bizantinos**.

**Não há solução na presença de 1/3 ou mais Generais Bizantinos**

Sendo **g** o número de generais, dado g = 3 no cenário abaixo o **General B** não consegue distinguir quem é bizantino, por isso são necessários menos que 1/3 de traidores para o sistema possa funcionar. 
<Mermaid chart={`
graph TD;
classDef byzantine fill:#555, stroke:#F00, color:#FFF, stroke-width:3px;
classDef honest fill:#555, stroke:#0F0, color:#FFF, stroke-width:3px;
    A:::byzantine;
    B:::honest;
    C:::honest;
    a:::byzantine;
    b:::honest;
    c:::honest;
    A(General A) ==> |atacar| B(General B);
    A ==> |recuar| C(General C);
    C ==> |recuar| B;
    b(General A) ==> |atacar| c(General B);
    b ==> |atacar| a(General C);
    a ==> |recuar| c
`}/>

---
**Comparação entre Generais Bizantinos e Blockchain**

| **Generais Bizantinos** | **Blockchain**           |
|------------------------------|--------------------------|
| Distância Física | Rede Distribuida |
| Generais | Nodes |
| Generais Bizantinos | Nodes Falhos ou maliciosos |
| Mensageiros não confiaveis | Mensagens descartadas ou corrompidas |
| Atacar, Recuar | Histórico de Consenso (Transações) |

## Consenso Baseado em Voto (Paxos & Raft)

### Parlamento de Paxos

- **Proponentes** - Legisladores que defendem o pedido de um cidadão e encaminham propostas.
- **Aceitantes** - Legisladores que votam nas propostas.
- **Alunos** - Alunos que "aprendem" as propostas que alcançou o **consenso** e divulgam para os cidadãos.

### Consenso de Paxos

Para chegar a um consenso a maioria deve votar na proposta. Uma maioria é chamada de **Quorum**.
Qualquer dois **quorum** devem se sobre por.

| **Parlamento de Paxos** | **Sistemas Distribuidos**           |
|------------------------------|--------------------------|
| Legisladores | Servidor |
| Cidadãos | Clientes |
| Lei Atual | Estado do Banco de Dados |

### Raft - Uma Alternativa ao Conseno de Paxos

**Raft** é um algoritmo de consenso alternaticvo ao conseenso de Paxos. Desenvolvido para ser mais fácil de compreender do que Paxos, e baseado em um líder. Funciona da seguinte forma:

- Cada cluster *raft* tem um e apenas um líder.
- O líder reponsável por orquestrar o envio de mensagens para outros nós dentro do cluster, além de replicar  e manter os log de tudo que acontece.
- O líder aceita uma solicitação de um cliente e em seguida supervisiona todos os outros nós para garantir que receberam a solicitação. Depois disso é replicado o log dessa solicitação.
- Quando um nó do cluster recebe uma mensagem ele inícia um *timer*, caso nenhum líder lhe envie uma mensagem até o fim desse *timer* o nó se "auto elege" e começa a enviar mensagens como novo líder.
- Se o íder falhar os nós elegem um novo líder através do processo anterior e continuam o processo.

Exemplo visual de como o processo [acontece](https://raft.github.io/)