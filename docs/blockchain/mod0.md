---
sidebar_position: 2
---

# Linha do Tempo.

## Ínicio do movimento
- Cypherpunks e CriptoAnarquista aparecem com o objetivo de proteger sua privacidade

## Digicash - David Chaum (1989)
- Primeira criptomoeda (centralizada)

## [Manifesto Cypherpunk - Eric Hughes 1993](https://www.youtube.com/watch?v=wJv6eymZvro)

## HashCash - Adam Back (1997)
- Primeiro Proof-of-Work  criado para evitar spans no email

## B-Money - Wei Dai (1998)
- Moeda criptografica muito parecida com [bitcoin](https://bitcoin.org) (síncrono)

## Bitcoin - "Satoshi Nakamoto" (2008)
- Bloco gênesis (03/01/2009):
    - "The Times 03/01/2009 - Chancekkor on brink of second bailout for banks"
    - ![img](/img/mod0/07Za4tR.png)
    - ![img](/img/mod0/PmYotJw.png)
    - ![img](/img/mod0/qNnV84y.png)
- Total de 21 Milhões de moedas apenas.
- Pizzaday 22/05/2010
    - ![img](/img/mod0/pizza.png)

## Falência por roubo da MT.GOX (2010-2014)
- 744,408 BTC

## Litcoin (2011)
- As Alticoins apareceram logo após o Bitcoin para resolver problemas particulares e ou novas demandas
- Litcoin (fork do Bitcoin) foi a primeira altcoin

## Silk Road (2011-2013)
- "Ebay of Drugs"
 site na deep web que vendia drogas e qualquer tipo de coisa legal ou não usando BTC como principal forma pagamento
 - Dono Preso e 26K BTC apreendidos pelo FBI

## Bitcoin Bolha (01/10/2013 - 30/10/2013)
 - preço do $200 para um pico de $1165 depois só se recuperou em 2015


## Ethereum Vitalik Buterin (2013)
- Blockchain de 2º geração: smartcontracts 
- Whitepaper lançado em 2013 e rodou pela primeira vez em 2015
- ETH chega a valer mais de $1B de dólares

## Compras com Bitcoin (2014)
- Paypal faz parcerias com:
    - Coinbase
    - BitPay
    - GoCoin

## Bancos e Blockchain (2014)
- Blockchain privadas e Ledger permisionados começaram a ser adotados como
- Corda R3 (2014)
- HyperLedger Linux Foundation (2015)

## StableCoin (2016)
- Lastreada em dólar 1:1
- Tether
- DAI
- Tronx

## DAO (2016)
- Organização Autônomas Descentralizada (DAO)
- "The DAO" era um projeto descentralizado que servia como um capital de risco descentralizado que permitiria que seus investidores votassem e decidissem sobre a distribuição dos fundos entre startups
- "The DAO" ficou conhecida como um dos maiores financiamentos coletivos da História, com 11K membros investiram mais de $150M

## DAO Hack (2016)
- O hack causou uma divisão na comunidade o que gerou o Ethereum e Ethereum Classic
- $70M de dólares roubados

## Millenials (2017)
- CryptoKitties um Dapp de troca de gatinhos virtuais chegou a usar 10% da rede ETH
- Congestionou a rede

## ICO (2017)
- Oferta Inicial de Moedas (ICO = IPO)
- Os ICOs e o "medo de perder o próximo BTC" arrecadaram cerca de $1,3B com 150 ICOs enquanto investimentos (seed ou angel) em todos os setores de tecnologia $1,4B em 1602 negócios

## ICOs Famosas (2017)
- Bancor $150M
- Tezos $200M
- Filecoin $253M

## Wallet Parity (2017)
- Um usuários "devops199" bloqueou $300M de dólares em contrato que não recebeu auditoria (testes) adequados

## Coincheck (2018)
- Maior hack da história das criptoativos mais de $500M de dólares

## DeFi (2019)
- Finanças Decentralizadas
- Conjunto de protocolos com o objetivo de fornecer produtos financeiros
- Integração entre diferentes tipos de protocolos: Money Legos
- Blockchain de 2º geração baseado em smartcontrats
- Casos famosos:
    - Compound: Empréstimos com colateral
    - Uniswap: DEXs
    - Yearn Finance
    - Pool toghter

## NFT (2019 - 2020)
- Token é uma representação digital de um artigo físico
- Podem ser particionados 

# Investimentos
## Como escolher Projetos
- Acompanha grandes investidores do projeto
- Entenda do que se trata o projeto
- Métrica dos Desenvolvedores do projeto 
- Entusiasta sobre projeto ver potência disruptivo da solução

## Scam e Fraudes
- Os golpes são sempre os memsmo mas com nomes diferentes
- Use redes sociais como tecnologia de pesquisa, proteção e estratégia
- Entenda o básico: Criptomoedas não dão retorno
- Golpe é fruto de má-fé com ganância exarcebada: Controle sua ganância
- O dinheiro é seu, então a culpa será sua: entenda muito be onde está pisando

## Alocação de Investimento
- "Vá com o dinheiro do pinga não do leito"
- Comece pela bolsa e vá evoluindo sua exposição de acordo com seu entendimento
- Faça 2 portifólios:
    - 1º compre regularmente com a meta de chegar a 1BTC (longo prazo)
    - 2º portifólio diversificado com vários ativos
- Total de 5% a 10% do seu patrimônio

