---
sidebar_position: 4
---

# Hash Function
[Hash Function](https://emn178.github.io/online-tools/sha256.html)

**O que é um hash?**

"*Aleatóridade Padronizada*"

Um algoritmo que mapea dados de tamanho variável para dados de comprimento fixo. Usando uma função do tipo *trap-door* que faz com que seja fácil calcular o resultado mas impraticável fazer o caminho inverso sem uma palavra passe. 

    [ok]  x   --->   f(x)        --->  y  
    [not] x   <---  f⁻¹(y)       <---  y  
    [ok]  x   <---  f(y+pass)    <---  y


**Propriedades**
- **Unidirecional**: dificuldade em envontrat a pré-imagem a partir da imagem.

- **Resistência a Segunda Pré-imagem**: dificuldade em encontratrr uma segunda pré-imagem que gere a mesma imagem.

- **Colisão**: dificuldade me encontrar duas pré-imagens diferentes com a mesma imagem

**Efeito Avalanche**

Isso significa que qualquer mudança na entrada leva a uma mudança pseudo-aleatória na saída. Não há relação entre os Hashes, apesar das entradas serem muito semelhantes.

## **Assinaturas: Public/Private Keys**

**Digital Signature Scheme (DDS)**

- **Origem da Menssagem:** Fácil indentificação do remetente
- **Não-Repúdio:** O remetente não pode retroceder a menssagem
- **Integridade da mensagem:** A menssagem não pode ser modificado após ser assinada

## **Matemática: ECDSA**


É uma função do tipo *trap-door* que quer dizer que ela é uniderecional sendo impraticável calcular sua inversa.
A usada no Bitcoin é a secp256k1 ou seja:

    y² = x³ + ax + b

Tem a função de *assinar* as transações de forma que seja fácilmente verificável o dono da mensagem.
Funciona básicamente como um relógio:
- Se eu trabalhei das 12h do dia 20 até às 13h, quantas horas eu trabalhei?
    - Se for referênte ao primeiro dia será 1h mas se for ao segundo será 25h

Baseado nesse conceito suponha que eu te diga: *"Meu número preferido é 3!"*. Logo depois lhe envio uma mensagem: *"Hoje não irei! - assinado Prof. 11"*.
Como saber se estou mentindo dado que você não sabe divisão?
- Se o número de letras da frase for multiplo de 3 será verdade.
- Se mão será mentira com certeza

Minha **chave privada** será quantas "voltas no relógio" foram precisas para encontrar o múltiplo.


**Criando Chaves**

secret key = n
public key = n*P = P + ... + P
address = RIPEMD160(SHA256(n*P))

    pub_key -> sha256() -> ripemd160 -> pub_key_hash -> base58_encode -> new_address


## **Blocos: Block Header**

**Exemplo de Bloco:**

![ex bloco](/img/mod2/example_block.png)

### **Conteúdo dos Blocos**

- **Block Header** (metadata)
    - **Makle Root** (Merkle Tree)
    - **Prev Block** (Blockchain)
    - **Nonce** (Proof-of-Work)
    - timestamp
    - target
    - version
- Block Size
- Transaction counter (length transaction)
- Transaction
- Block Id

### **Merkle Root: Merkle Tree**

A Merkle Root (Raiz de Merkle) é o topo de uma árvore binária de Merkle, que é um dado criptográfico estrutura em forma de (adivinha) árvore.

Em primeiro lugar, uma árvore em Ciência da Computação é uma estrutura de dados que tem algum nó raiz e alguns filhos, que também podem ser raízes de outras árvores, uma árvore binária é uma árvore em que cada nó tem no máximo dois filhos ou ramos (branchs):

                         branch3
                        /
                 branch2
                /       \
          branch1        branch3
         /       \      
        /         branch2
    root       
        \         branch2
         \       /      
          branch1         branch3  
                 \       /
                  branch2




Uma árvore Merkle é apenas uma versão muito específica de uma árvore binária, onde duas coisas são verdadeiras.
Talvez a melhor maneira de explicar a Árvore Merkle seja descrever sua construção. Começamos com um conjunto de transações que verificamos. Nós os colocamos um por um e fazemos o hash de cada uma para obter um nível de hashes (branch3).
Em seguida, misturamos cada par, criando um novo nível com a metade dos hashes (branch2). Continuamos esse processo até que finalmente temos apenas um hash no topo (Root). Este hash no topo é a raiz Merkle, dessa forma, podemos detectar qualquer alteração na transação após o está ter sido gravada (commit) com a Raiz Merkle.

## **Previous Hash: Blockchain**

A Blockchain é a cadeia de blocos linkados sempre ao seu antecessor por um identificador (hash). Desse forma é possível rastrear adulterações nos dados começando na Merkle Tree e por fim através dos *previous hash* até o bloco "fraude".

## **Nonce: Proof-of-Work**

- Computacionalmente Difícil
- Parametrizável (dificuldade ajustável)
- Fácil Verificação

**Quebra-Cabeça**

O Hash do bloco deve começar com um determinado número: **#0000a23b4acb456...** Dessa forma todos os nós tem a probabilidades iguais em acertar o alvo.
A única maneira de sair na frente é jogando mais vezes o mais rápido possivel. A dificuldade é dado pelo número de **zeros** que iniciam o hash ex:

    Fácil:        001a3c32d34e864b645d
    Difícil:      00000002a23d3e785f47

**Dificuldade**

É recalculada a cada 2016 blocos, não pode-se aumentar ou diminuir em fator de 4 vezes e é calculado multiplicando a dificuldade atual (nBit) pela razão do timestamp equivalente a duas semanas pelo tempo gasto para minerar 2016 blocos, ex:

    dificulte *= two_week / time_to_mine_prev_2016_blocks

**Usando o Código:**

    nBit = 486604799
    hex(nBit)               # 1D00FFFF
    (1D)                    # 29
    exp = 29
    int(00FFFF)             # 65535
    base = 65535
    k = base * 256^(exp - 3)
    hex(k)                  # FFFF000...
    len(hex(k)) / 2         # 28
    x - 28                  # x é Nº de zeros à esquerda


**Calculando dificuldade Atual**

    b_0 = last                     # timestamp do bloco atual
    b_f = b_0 - 2015               # timestamp do bloco 2015 atrás
    time = b_0 - b_f
    2week = timestamp(2week)       # duas semanas em timestamp
    nBit = targetcurrent 
    
### **Coinbase**

A transação **coinbase** acontece sempre que um minerador encontra o nonce que valida o bloco, então ele adiciona a transação ao início da Merkle Tree e envia o bloco para rede.   


## **Transações: P2PKH/P2PSH**

- *"Transações são a parte mais importante do sistema bitcoin. Todo o resto é desenhado para garantir que as transações possam ser criadas, propagadas na rede, validadas, e finalmente ao livro-razão de transações (a blockchain). Transações são estruturas de dados que codificam a transferência de valor entre participantes do sistema bitcoin. Cada transação é uma entrada pública na blockchain do bitcoin, o livro-razão de dupla entrada global." - Mastering of Bitcoin*

As transações compoẽm o coração do bitcoin, então merecem uma atenção particular como também um cuidado maior devido ao seu nível de complexidade, vamos por partes.

**UTXO**

No bitcoin não há contas associadas as suas transações, em vez disso cada transação é resultado de outras transações. O que se transfere de fato é o direito de *gastar* suas UTXO ou Bitcoins. Na verdade *gastar*  bitcoins é o ato de resgatar as transações anteriores como único dono legitimo por meio de uma **prova** e em seguida especificar um novo dono que poderá resgatar essa mesma transação e repetir o processo.


                                      -> input(0.9)
    -> input(2)                     /    output(x) ------> ...
       outpu(1.9) ----> input(1.9) /    
                        output(0.9) 
                        output(0.9)
                                   \
                                    \   
                                      -> input(0.9)
                                         output(x) ------> ...

**Estrutua de uma Transação**


Transação:

    {
        "hash":                 "ID DESSA TRANSAÇÃO",
        "ver":                  "VERSÃO DO SOFTWARE DO BITCOIN,
        "vin_sz":               "TAMANHO DO VETOR DE ENTRADA",
        "vout_sz":              "TAMANHO DO VETOR DE SAÍDA",
        "lock_time":            "TEMPO DE TRAVA",
        "size":                 "TAMANHO DA TRANSAÇÃO (bits)",

        "in": [ // VETOR DE ENTRADAS
            {
                "prev_out": {
                    "hash":     "ID DA TRANSAÇÃO",
                    "n":        "INDEX DESSE INPUT(output) NA TRANSAÇÃO ANTERIOR"
                },
                "scriptSig ":   "PROVA DA POSSE"
            },
        ],

        "out": [ // VETOR DE SAÍDAS
            {
                "value":        "NÚMERO DE SATOSHIS",
            //  TIPO DE PROVA
                "scriptPubKey": "SCRIPT PARA A PROVA DE POSSE"
            }
        ]
    }

**Scripts**

É uma linguagem projetada para processar as transações do bitcoin, com ela as transações podem acontecer de forma mais complexas e as fucionalidades do bitcoin podem ser extendidas. Bitcoin conecta transações em *scripts* em vez de chaves públicas para permitir transações potencialmente mais complexas. Essa linguagem de *script* é baseada em pilha e tem suporte nativo para criptomoedas e é **propositalmente** ela é similar a [Forth](https://pt.wikipedia.org/wiki/Forth) e é limitada - Turing incompleta - para evitar problemas inesperados na rede e garantira robustez das transações.

### **Bitcoin Scripts**


**new ouput(n)**

```
    <sig>
    <pubKey>
```
**prev input(n)**
```
    OP_DUP
    OP_HASH160
    69e023...
    OPEQUEALVERIFY
    OPCHECKSIG
``` 
1
```
<sig>
```
2
```
<pubKey>
<sig>
```
3 --> 4
```
[OP_DUP]            <pubKey>
<pubKey>    --->    <pubKey>
<sig>               <sig>
```
5 --> 6
```
[OP_HASH160]                <pubKeyHash>
<pubKey>        --->        <pubKey>
<sig>                       <sig>
```
7
```
<pubKeyHash?>
<pubKeyHash>
<pubKey>
<sig>
```
8 --> 9
```
[OP_EQUALVERIFY]
<pubKeyHash?>
<pubKeyHash>        --->    <pubKey>
<pubKey>                    <sig>
<sig>
```
10
```
[OP_CHECKSIG]
<pubKey>            --->    true : false
<sig>
```


### **P2PSH**

Essa é o tipo de prova mais simples para uma transação, nela o *lockingscript* e criado com a chave pública do destinatário e esse deve usa sua chave pública e sua assinatura para no *unlockingcript* afim de desbloquear essa transação e poder "gasta-la"

### **MultSig**

Nesse tipo de esquema de *lockingscript* são usadas várias assinaturas para destravar os UTXO, mais especificamente *m* de *n*, o que quer dizer, por exemplo, que são necessárias 3 de 5 chaves para desbloquear um certo UTXO. Esse recurso fornece um meio de criar transações mais complexas e mais seguras.

### **P2SH**

Uma transação P2SH usa como *lockingscript* o hash (HASH160) do que seria um *lockingscript* comum para poupar processamento, armazenamento e facilitar a usabilidade do lado do pagador. O P2SH pode ser também em um endereço ou chave pública, condensando assim todo o *lockingscript* em um endereço. Esse tipo de endereço começa com digito "3"

