---
sidebar_position: 6
---

# Contratos Inteligentes

Contratos são acordos que buscam alguma forma de concenso seja por **Lei** ou **Algorítmos**.

- Contratos: um acordo escrito ou falado que se destina a ser **EXECUTADO POR LEI**.

- Contrato Inteligente: um pedaço de código que facilita, verifica ou impõe a **EXECUÇÃO** de um contrato digital por meio de uma **ENTIDADE CONFIÁVEL**.

## Evolução de Protocolos

**Bitcoin**

- *Padrão Bitcoin*: Padrão Ouro dos protocolos blockchain .
- Ativo: *bitcoin* tem **apenas** a finalidade de permitir transações.
- Simples e Robusto
- Linguagem de Script baseada em Pilha que não é Turing-completa.
- Baseado em UTXO.
- Consenso baseado em Proof-of-Work.

**Ethereum**

- *Contratos inteligentes*: plataforma blockchain
- Ativo: *ether* tem finalidade **geral** de financiar computação e alinhar incentivos.
- Complexo e Rico em Recursos.
- Linguagem de script Turing-completa **Solidity**.
- Baseado em Contas.
- Consenso baseado em Proof-of-Work

## Introdução ao Ethereum

Na rede Ethereum a chave privade da acesso a uma conta, diferente da rede Bitcoin que nos dava acesso aos UTXO. Existem dois tipos de Contas Propriedade Externa e Contas de Contratos.

**Contas Propriedade Externa**
- Contém propriedades de alguma entidade externa como: pessoas, grupos de pessoas ou corporações, etc.
- Podem conter um endereço para fazer ou receber transações, acionar contratos.
- Recursos:
    - Endereço
    - Saldo de Ether.

**Contas de Contratos**
- São propriedades de contratos inteligentes.
- Tem seu código executado quando contas de propriedade externa ou outras contas de contrato fazem transações para acionar as chamadas de função de seu código.
- Recusos:
    - Armazenamento persitido
    - Endereço
    - Code de contrato associado


Contratos inteligentes são como agentes autonômos dentro da rede Ethereum, eles tem controle direto ao saldo de ether, estado interno e ao armazenamento permanente de uma conta de contrato.

Os Contratos Inteligentes servem a alguns propósitos gerais como:
- Armazenar e manter dados
- Gerenciar contratos ou relações entre usuários
- Fornecer funções para outros contratos
- Ajudar em casos de autentificações complexa.

## Máquina Virtual do Ethereum

Os contratos inteligentes podem ser escritos em [Solidity](https://github.com/ethereum/solidity) ou [Vyper](https://github.com/vyperlang/vyper). Essas linguagens compilam para código da *Máquina Virtual Ethereum* ou [EVM](https://github.com/ethereum/go-ethereum) que por suaa vez será executado em cada nó da rede Ethereum. A rede Ethereum funciona como um computador distribuido que busca o consenso do estado atua do sistema, diferente da rede Bitcoin que busca um consenso sobre o estado de quem possui quais UTXOs, aqui o consenso dos nós está focado no estado geral da rede.

A prova de trabalho na rede Ethereum é semelhante a do Bitcoin, porém ela afirma ser resistente a ASICs e fecha blocos a cada 15 segundos. O algoritmo de hash é o [Ethash](https://eth.wiki/en/concepts/ethash/ethash).
[HashRate](https://bitcoin.org/en/vocabulary#hash-rate) atual das redes bitcoin e ethereum em [06-06-2021](https://bitinfocharts.com/comparison/hashrate-btc-eth.html):

- **Bitcoin:** 129.4503 *Exabytes*
- **Ethereum:** 562.0673 *Terabytes*

O código EVM é um bytecode de baixo nivel como o bytecode da JVM que usa um [gas](https://ethereum.org/pt-br/developers/docs/gas/) como "combustível" para evitar ataques de DDOS e loops infinitos inseridos na programação dos contratos inteligentes. O custo desse gas é dado em [Ether](https://ethereum.org/pt-br/eth/) que é cobrado de quem invoca o contrato, caso seja adicionado gas em excesso, o excedente é devolvido ao cliente e se não for suficiente o gas fica como pagamento pelo esforço computacional do nó de executar o contrato.

O objetivo principal da rede não é fornecer computação distribuida de forma eficiente mas permitir computação descentralizada [sem confiança](https://academy.binance.com/en/glossary/trustless). A rede é redundanteemente paralela fazendo com que todos os nós executem os mesmos cálculos para chegar um consenso sobre o estato geral da rede, eliminando assim o "intermediário de confiança"


## Aplicações e Casos de Uso

- Criação de Ativos e Tokens
- Multiplas assinaturas
- Provas de existência *(DNS)*
- Prova de propriedade de documentos *(escrituras)*
- Previsão de mercados *(eleições)*
- Cadeia de suprimento *(GIGO)*
- Comercio 24/7 *(tokenização)*

**Potência caso de uso na agricultura**


Com as mudanças climáticas e o aumento da ocorrência de condições climáticas extremas, os agricultores estão cada vez mais propensos a tais eventos. Por exemplo, se suas safras forem expostas a chuvas fortes ou secas extremas, eles podem perder todas as suas safras e receitas naquele ano. Isso pode ameaçar a existência dos agricultores. Conseqüentemente, muitos agricultores podem ter interesse em adquirir um produto de seguro. No entanto, os problemas que os agricultores enfrentam atualmente são a avaliação subjetiva dos danos e o atraso nos pagamentos. Por exemplo, enquanto um agricultor pode alegar que perdeu todas as suas colheitas devido a uma seca severa, a seguradora pode não estar disposta a pagar, alegando que o agricultor usou um fertilizante ruim. Em suma, a maioria dos pequenos proprietários não pode contestar a decisão de pagamento da seguradora no tribunal e tem que confiar nos padrões de justiça da seguradora. Contratos de seguro inteligentes executados em um blockchain podem ajudar a resolver esses pontos problemáticos. Em vez de pagar uma quantia subjetiva que supostamente compensaria a perda de receita, o contrato inteligente acionará automaticamente um pagamento quando um evento climático extremo ocorrer no local do fazendeiro. Conseqüentemente, o pagamento pode sub ou sobrecompensar o agricultor, no entanto, ele pode ter certeza de que, no caso de um evento E de gravidade S, ele receberá um pagamento no valor de P. Isso cria transparência. Ethereum oferece suporte a uma linguagem de script completa, que permite a criação de código arbitrário. Portanto, o contrato inteligente é escrito de forma que os valores de pagamento sejam baseados em eventos predefinidos. Para o contrato inteligente, isso significa simplesmente acionar B se A ocorrer. O agricultor teria que fazer pagamentos regulares para o seguro, que são validados por meio do processo de prova de trabalho do blockchain e, em seguida, receber pagamentos automáticos em caso de eventos específicos, sem ter que fazer reivindicações de seguro a uma entidade central. Claro, algum oráculo é necessário para alimentar os dados meteorológicos para o contrato inteligente (portanto, ainda há algum grau de centralização - discutirei isso mais tarde). Oráculos inteligentes podem ser usados ​​para automatizar esse processo.

Mas quais recursos do blockchain o tornam mais aplicável a este caso de uso em comparação a um banco de dados centralizado? Primeiro, permite uma solução tecnológica para um problema social. Agricultores e seguradoras podem ter interesses conflitantes. No mundo real, a informação assimétrica muitas vezes leva à seleção adversa, por meio da qual apenas os agricultores que correm um alto risco de serem afetados por condições climáticas adversas procuram cobertura de seguro porque os termos são atraentes apenas para eles. No entanto, ao mudar de pagamentos baseados em danos para pagamentos baseados em eventos, e dada a disponibilidade de dados históricos, o problema de seleção adversa pode ser resolvido. Assim, os contratos inteligentes e o blockchain permitem uma verificação rápida das transações para que os pagamentos possam ser feitos rapidamente. Em segundo lugar, e mais importante, o blockchain serve ao propósito de pura descentralização neste contexto. Uma vez que os contratos inteligentes definem claramente os eventos de pagamento, os agricultores não devem mais confiar em uma entidade central (o provedor de seguro). Isso evita a corrupção centralizada, por meio da qual o segurador aproveita a subjetividade para avaliar os sinistros de seguro. Em vez disso, os contratos inteligentes impõem a execução do contrato digital, portanto, a aplicação por meio da lei não é mais necessária, uma vez que o contrato lida com o relacionamento entre as partes não confiáveis. Terceiro, o blockchain é à prova de adulteração, então quase não há possibilidade de qualquer parte manipular o contrato inteligente sem ser detectado. Em vez disso, existe um registro imutável de data e hora do dia. Essa segurança elevada libera os provedores de seguros da tarefa de verificar reclamações de seguros e monitorar outras formas de fraude. O mecanismo de pagamento confiável e transparente pode até aumentar a satisfação do cliente. Quarto, o armazenamento distribuído em blockchain torna a perda de dados (o que é uma preocupação no caso de armazenamento centralizado, uma vez que as seguradoras lidam com grandes quantidades de dados) muito improvável.

No entanto, usar o blockchain para seguro de safra também tem inúmeras desvantagens. Primeiro, a execução do contrato é cara, pois todo contrato precisa de gás. Portanto, também pode ser possível implementar esse modelo de seguro off-blockchain. A centralização pode permitir uma maior eficiência devido a menos redundância (por exemplo, cálculos). O julgamento humano pode ser necessário em caso de eventos climáticos complexos (por exemplo, se vários eventos ocorrerem ao mesmo tempo). Além disso, o tratamento de erros humanos não é possível no blockchain (por exemplo, se dados incorretos de entrada de tempo forem detectados em retrospectiva). Em segundo lugar, a necessidade de oráculos para fornecer dados meteorológicos do mundo real para o contrato inteligente é uma preocupação por duas razões: (a) o problema GIGO com dados de entrada incorretos (por exemplo, devido a um sensor meteorológico com defeito) pode levar a pagamentos errados que não pode ser revertido; (b) oráculos são uma forma de centralização e, portanto, um único ponto de falhaUre: Os atacantes, em vez de manipular o contrato inteligente, podem atacar a organização que fornece os dados meteorológicos. Uma vez que os pagamentos são quase imediatos (o tempo de bloqueio atual no Ethereum é de cerca de 13 segundos) e o blockchain é imutável (EVM executa o código do contrato inteligente e muda o estado), as ações realizadas por agentes maliciosos são principalmente irreversíveis. Terceiro, os pequenos proprietários podem não achar muito útil serem compensados na forma de criptomoeda. Além disso, os agricultores em países menos desenvolvidos podem não ter o hardware ou acesso à Internet para se tornarem parte de um seguro de contrato inteligente descentralizado executado em um blockchain.

## Ecosistema Blockchain

**Blockchain vs Internet**

O ambiente social onde nasceua internet não era provido de computadores pesoas como o do blockchain, por isso seu uso er avoltado as *intranets* como as blockchains permisionadas.
Ambos são protocolos de comunicação, que necessitam de um meio seguro para funcionar.

| Blockchain 	| Internet 	|
|-	|-	|
| Troca de **valores**  	| Troca de **informações** 	|
| Informações **públicas** 	| Informações **privadas** 	|comunicação 	|
| **Surgiu** de projetos *open source* depois foi para a acadêmia e empresas 	| **Surgiu** na acadêmia depois foi para as empresas e por fim para o público 	|

**Quando um Blockchain funciona mas não é necessário?**

- "Eficiência": depende do contexto
- Não é exclusividade do Blockchain:
    - Imutabilidade dos dados
    - Integridade, autenticidade e auditabilidade
    - Tolerante a falhas
    - Criptografia de chaves públicas

**Resolvendo problemas de forma Coordenada**

- Permite criar incentivos arbitrários
- Solução para problemas sociais
- Sistemas que operam sem confiança em terceiros
- Construções e financiamentos soletivos

**Integração Horizontal**

- Elimana combinação de dados em silos
- API e padrão de dados forçadamente comuns
- [Efeito de rede](https://pt.wikipedia.org/wiki/Efeito_de_rede)
- Aplicações e dados totalmente abertos

**Descentralização**

- Resistente a censura
- Rede aberta
- Reconhecimento Global de propriedade
- Contorna a corrupção centralizada
- Sistema tolerante a [falha bizatina](https://en.wikipedia.org/wiki/Byzantine_fault)

