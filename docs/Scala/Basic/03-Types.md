---
sidebar_position: 2
---
import Mermaid from '@theme/Mermaid';

# Tipos

---
### Tipos de Variável

Em Scala, como já foi dito, *`var`* declara uma variável mutável enquanto *`val`* declara uma variável imutável, essas variáveis podem ser de diferentes tipos, o compilado *Scala* consegue inferir os tipos
mas declara-los pode ser muito útil como documentação e prevencão de erros. Exemplos de tipos:

| Tipo | Tamanho | Intevalo |
| --- | --- | --- |
| Unit | 1bit | () |
| Boolean| 1bit | true ou false |
| Byte | 8bits | -128 : : 127 |
| Char[^1] | 16bits |0 : : 65535 |
| String[^2] | Seq[Char] | - - - |
| Short | 16bits | -32768 : : 32767 |
| Int | 32bits | -2147483648 : : 2147483647 |
| Long | 64bits | -9.223372036854776E18 : : 9.223372036854776E18 - 1 |
| Float[^3] | 32bits | [IEEE-754](https://www.youtube.com/watch?v=PDgT0T0Yodo) -1.40129846432481707e-45 : : 3.40282346638528860e+38 |
| Double[^4] | 64bits | [IEEE-754](https://www.youtube.com/watch?v=PDgT0T0Yodo) 4.94065645841246544e-324d : : 1.79769313486231570e+308d |


Os valor podem ser convertidos da seguinte maneira:

<Mermaid chart={`
	graph LR;
		Byte --> Short --> Char --> Int --> Long --> Float --> Double;
        Int --> Char;
`}/>

---
### Hierarquia de tipos

- Em *Scala* todos os valores tem um tipo, incluindo valores numéricos e funções e todos eles derivam do 		tipo *`Any`*
- Apartir de *`Any`* temos *`AnyVal`* e *`AnyRef`*. Cada um *superclass* de um conjunto de valores ou 			elementos.
	<Mermaid chart={`
		graph TD;
					Any --> AnyVal & AnyRef;
	`}/>

- Todos os tipos de **valores** derivam de *`AnyVal`*. Em Scala não **existe tipos primitivos**.
<Mermaid chart={`
	graph TD;
		AnyVal --> Double & Float & long & Int & Short & Byte & Unit & Boolean & Char;
`}/>

- De *`AnyRef`* surgem todos os outros tipos de entidades como classes e funções. *`AnyRef`* é compativel a *`java.lang.Object`*.
- *`Null`* é derivado de *`AnyRef`* para gerar compatibilidade com outras linguagens da **JVM**.
<Mermaid chart={`
	graph TD;
		AnyRef --> Collection & Option & YourClass --> Null;
`}/>

- Por fim o tipo base de todos os tipos é o *`Nothing`*.
<Mermaid chart={`
	graph TD;
		Any --> AnyVal & AnyRef;
		AnyVal --> Nothing;
		AnyRef --> Null --> Nothing;
`}/>

- O diagrama completo fica assim:
<Mermaid chart={`
	graph TD;
		Any --> AnyVal & AnyRef;
		AnyVal --> Double & Float & long & Int & Short & Byte & Unit & Boolean & Char --> Nothing;
		AnyRef --> Collection & Option & YourClass --> Null;
		Null --> Nothing;
`}/>

---
### Nothing and Null

**Nothing** é um subtipo de todos os tipos, também chamado de tipo inferior. Não há valor com o tipo **Nothing**. Um uso comum é sinalizar o não encerramento, como uma exceção lançada, saída do programa ou um loop infinito (ou seja, é o tipo de uma expressão que não avalia um valor ou um método que não retorna normalmente).

**Null** é um subtipo de todos os tipos de referência (ou seja, qualquer subtipo de **AnyRef**). Ele possui um único valor identificado pela palavra-chave literal *`null`* do tipo *`Null`*. **Null** é fornecido principalmente para interoperabilidade com outras linguagens JVM e quase nunca deve ser usado no código Scala.


[^1]: aspas simples
[^2]: aspas duplas
[^3]: precisão simples
[^4]: precisão dupla
