---
sidebar_position: 3
---

# Funções

### Funções e Métodos

Funções podem ser declaradas usando a palavra chave *`val`*. As funções podem ter muitos parâmetros ou nenhum. As funções também podem fazer parte da expressão de outra função.

- **Anonima**
  ```scala
  (x: Int) => x + 1
  ```
  - **Nomeadas**
  ```scala
  val somaUm = (x: Int) => x + 1
  ```
  - **Sem Parametros**
  ```scala
  val retornaUm = () => 1
  ```
- **Com outra Função**
  ```scala
  val adicionaUm = (x: Int) => x + 1
  val retornaUm = () => adicionaUm
  ```

Já os métodos precisam ser declaradas usando a palavra chave *`def`*. Eles podem ter várias listas de parâmetros (ou nenhuma). Um tipo de retorno deve ser especificado após lista de parâmetros. Em Scala é má prática usar a palavra reservada *`return`* para retornar. 

  - **Sem parametros**: São funções chamadas de funções com *efeito colateral* pois não retornam ou recebem nenhum valor mas alteram estados do código.
  ```scala
  def minhaFuncao: Unit = println("Funcionou!")
  minhaFuncao
  // Funcionou!
  ```

  - **Com Parametros**: Os parametros devem ter os tipos declarados 
  ```scala
  def funcaoInteiros(x: Int, y: Int): Int = x + y
  minhaFuncao(2, 5)
  // 7

  def funcaoStrings(x: String, y: String): String = x + y
  minhaFuncao("Receita Federal = ", "Leão")
  // Receita Federal = Leão
  ```
