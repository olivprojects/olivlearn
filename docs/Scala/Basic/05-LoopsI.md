---
sidebar_position: 4
---

# Loops I

## Loops *for*

### Ranges

Os *`Range`* são intervalos que podem ser utilizados como iteradores. São definidos usando 3 parametros (início, fim, incremento). Para um intervalo inclusivo use *`to`*, para um intervalo exclusivo *`until`* e para o incremento use *`by`*.

```scala
val intervalo = (0 to 10 by 1)
for(n <- intervalo) print(s"$n, ")
// 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

for(i <- 10 until 0 by -1) {
    print(s"$i -> ")
}
// 10 -> 9 -> 8 -> 7 -> 6 -> 5 -> 4 -> 3 -> 2 -> 1 ->
```


Loops *`for`* retornam *`Unit`* e executam *efeitos colaterais*.
```scala
for(list) block

for(n <- 1 to 3) println(n)
// 1, 2, 3
```


## Expressões *for* *yield*

Expressões `for` são loops mas retornam um resultado, esse resultado é uma sequência *`Seq`* que pode conter **generators**, **filters**, **definitions**, expressões criam um elemento do resultado usando a palavra reservada `yield`.

```scala
seq = for(list) yield expression

val seq = for(n <- 1 to 3) yield n + 1
// Vector(1, 2, 3)
```

### Geradores

**generators** ou geradores se movem sobre a iteração e o primeiro elemento gerado determina o tipo do resuldado.

```scala
(elem <- collection)

for(n <- Set(1,2,3)) yield n + 10
// Vector(11, 12, 13)
```

### Multiplos *generators*

Multiplas estruturas de dados podem ser iteradas em estilo aninhado, ou seja multiplos geradores podem ser criados usando ponto e virgula ou em um bloco separados por uma nova linha.

```scala
{
    elements <- collection
    elem2 <- elements
}

for {
  x <- 0 to 3
  y <- 0 to x
} yield (x, y)
//Vector((0,0), (1,0), (1,1), (2,0), (2,1), (2,2), (3,0), (3,1), (3,2), (3,3))
```

### Filtros

Os filtros controlam a iteração e precisam ser avaliados como um *boolean*, eles também podem seguir os geradores na mesma linha.

```scala
elem <- collection if expression

for {
  n <- 1 to 10 if n % 2 == 0
} yield n
// Vector(2, 4, 6, 8, 10)
```

### Definições

Definições são variáveis locais que podem ser utilizadas na mesma linha.

```scala
x = expression

for {
  time <- times
  hours = time.hours
} yield hours
// Vector(1pm, 2pm, 3pm)
```

## Loops vs Expressões: *for*

Dependendo do momento pode ser mais útil usar um *`for`* como loop ou um simples *`range`*, o que deve influênciar a escola é a **legibilidade**.


```scala
// Ex1
for(n <- 1 to 3) yield n + 1

(1 to 3).map(n => n + 1)


// Ex2
for {
  n <- 1 to 3
  m <- 1 to n
} yield n * m

(1 to 3).flatMap(n => (1 to n).map(n => n * m))


// Ex3
for(n <- 1 to 3) println(n)
(1 to 3).map(println)
```
