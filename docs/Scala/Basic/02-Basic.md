---
sidebar_position: 1
---

# Início

---
### Val, Var, Lazy

- Em Scala *`var`* declara uma variável mutável enquanto *`val`* declara uma variável imutável.

  ```scala
  // Imutáveis
  val nacimento: String = "30/04/1997"
  val nome: String = "Lucas"

  // Mutáveis
  var idade: Int = 23000
  var altura: Double = 1.45
  ```

- Você pode usar *`lazy`* para inicializar uma variável *`val`* apenas no momento em que ela é chamada.

  ```scala
  // Lazy
  lazy val x: Int = 321
  ```
---
### Input, Output

- Podemos usar *`print`* e *`println`* para imprimir dados.
- Para colher dados da linha de Comando é preciso importar a função *`readLine`* no início do programa.
  ```scala
  print(1,2,3)
  // 123

  println(1,2,3)
  // 1
  // 2
  // 3 

  import scala.io.StdIn
  val saldacao = "Seu Nome? "
  val input = StdIn.readLine(saldacao)
  println(f"Olá $input!")
  ```
---
### String Multlinhas, Interpolação, Fstring, Raw

- Usar *`"""`* resolve o problema de formatação na tela.
- o simbolo *`$`* permite imprimir variáveis dentro de strings.
- *`${}`* é usados para executar expressões em strings.

  ```scala
  val s = """String
  de
   varias
    linhas"""
  //
  //String
  //de
  // varias
  //  linhas

  val seqLetra = "Frase Qualquer"
  val stringInterpolada = s"Salvando uma $seqLetra"
  // Salvando uma Frase Qualquer

  val a = 1
  val b = 2
  val expressoesInterpoladas = s"Soma: $a + $b = ${a + b}"
  // Soma: 1 + 2 = 3
  ```

- Para arredondar numeros em string usamos *`%.Nf`* onde *`N`* é o numero de numeros atras do ponto.
- A palavra reservada *`raw`* permite que simbolos sejam impressos.


  ```scala
  val pi: Double = 3.1415987
  val fString = f"Pi: $pi%.2f"
  // Pi: 3,14
  val fString = f"Pi: $pi%.3f"
  // Pi: 3,141
  val fString = f"Pi: $pi%.4f"
  // Pi: 3,1415

  val raw = raw"\nLinha 1\nLinha 2"
  //\nLinha 1\nLinha 2
  ```
---
### Blocos

- As condições *`if/else`* sempre retornam algo, então não precisamos de operadores ternários.
- Chaves *`{}`* podem ser usadas para isolar instruções do resto do código

  ```scala
  import scala.io.StdIn.readLine

  val presidenteAtual = readLine("Qual o presidente atual? ")

  val messagem = if(1 == 1) s"Fora $presidenteAtual" else "Apoio!"

  val x = {
    val y = 30
    val z = 20
    y + z
  }
  ```
