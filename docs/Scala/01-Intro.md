---
sidebar_position: 1
---

# Introdução

### Visão Geral

Escala é uma linguagem elegante e consica, alumas de suas características são:

- É uma linguagem de **Alto Nível**.
- É **Estaticamente Tipada**.
- Sua sintaxe é **concisa**, **expressiva**.
- Suporta **Orientação a Objetos**.
- Também suporta **Programação Funcional**.
- Possui um sofisticado **Sistema de Inferência de Tipos**.
- É uma linguagem compilada, que gera bytecode para **JVM**.
- Fácil **Interoperabilidade** com bibliotecas **Java**.
- **Expression-Oriented** Scala é orientada a expressão isso quer dizer sempre retornar algo.
- **MultLines** essas expressões podem ser mult-linhas.
- **Blocks** expressões podem ser criadas em blocos usando `{}`. Em blocos a última linha será o retorno do bloco.

**Lightweight**
- Ela também é uma *linguagem leve (lightweight language)* isso quer dizer que:
    - Não é preciso usar `chaves {}` para expressões de um linha.
    - Notações de tipos podem ser omitidas.
    - `Pontos .` e `parenteses ()` também podem ser omitidos.
    - Afimações `return` não são necessárias.


### Java Virtual Machine *JVM*

Máquina virtual Java é o software responsável por executar seu código. Ele traduz seu código para um outra liguagem chamada *bytecode*, apartir dai podemos executa-lo em qualquer lugar que tenha a JVM instalada.

### Simple Build Tools *SBT*

Sbt é uma ferramenta de construção de código para projetos **Scala** e **Java**, semelhante ao Maven e Ant do Apache. Suas principais características são: código aberto, suporte nativo para compilar o código Scala e integrar-se com muitas estruturas de teste, compilação e implantação. É usado massivamente pela comunidade Scala.

### Pré-Requisitos

Para começar a escrever código em *Scala* precisamos da JVM, e o binário da linguagem e posteriormente do sbt.

- **[Instalar jdk-8](https://openjdk.java.net/install/)**

- **[Instalar Scala](https://www.scala-lang.org/download/)**

- **[Instalar Sbt](https://www.scala-sbt.org/1.x/docs/Installing-sbt-on-Linux.html)**


O famoso *Olá mundo* fica assim: 

```scala
Object Hello extends App {
    println("Hello, world")
}
```

Depois é só salvar como `Hello.scala` e compilar com `$ scalac Hello.scala`.
Por fim para executar usamos `$ scala Hello`


### REPL

O **Scala REPL** (Read-Evaluate-Print-Loop) é um interpretador de linha de comando.
Ele é muito útil como *playground* para testar seu código Scala. Caso você tenha instalado os pré-requisitos do início
basta digitar scala na linha de comando do sistema operacional e você verá algo assim:

```
$ scala
Welcome to Scala 2.13.0 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_131).
Type in expressions for evaluation. Or try :help.

scala> _
```
