---
sidebar_position: 2
---

# Objetos

## Singleton

Os objetos que substituem elementos estáticos eles criam apenas uma única instância de uma classe e sua aplicação é um objeto *singleton* com um método main. 
Objetos Singleton são um tipo de **mescla e abreviação** para definir uma classe de uso único, a qual não pode ser diretamente instânciada. São ótimos para *Util Classes*, *Class Factories*, *Companion Objects* e *Applications*.

## Companion Objects

Quando um objeto *singleton* e uma classe ou um *trait* compartilham o mesmo **nome**, **pacote** e **arquivo**, eles são chamados de **companions** ou companheiros, nesses casos eles podem acessar os seus membros *`private`* entre si.

```scala
  object Hello {
    private val msg = "foi"
  }

  class Hello(msg: String = Hello.msg) {
    println(msg)
  }
```

## *Packages* & *Imports*

### Packages

Pacotes são organiradores de bases de códigos importantes e reusáveis. Para que posa usado de forma produtiva a estrutura do arquivo deve refletir a estrutura do pacote, por exemplo, para os códigos no diretório `src/main/scala/my/math/Matematica.scala`, o pacote deve ser declarado `package my.math.Matematica` dentro dos arquivos que serão "empacotados".

```scala
package my.math.Matematica

import scala.math.{pow, sqrt}


Object Math {

  def circunferencia(r: Double): Double = {
    import scala.math.Pi
    2.0 * Pi * r 
  }

  def areaCirculo(r: Double): Double = {
    Pi * pow(r, 2) 
  }

  def pitagoras(x: Double, y: Double): Double = {
    sqrt(pow(x, 2) + pow(y,2))
  }
}
```

### Imports

`package com.exemplo.package`,
O `import` é usado quando não se quer usar todo o pacote, já quando todos os membros do pacote são necessários é possivel importa-los de uma vez com `_`. Já para importar multiplos membros use `{membro1, ..., membroN}`, outra possibilidade é dar apelidos aos membros usando `{membro => apelido}`. Ponto importante os **Imports** podem ser restringidos a escopos especificos como:

```scala
import com.math.Matematica.Math.{circunferencia => circ}
import com.math.Matematica.Math.{areaCirculo}
import com.math.Matematica._

circ(2)
// 12.566370614359172

areaCirculo(4)
// 50.26548245743669

pitagoras(3, 4)
// 5.0
```