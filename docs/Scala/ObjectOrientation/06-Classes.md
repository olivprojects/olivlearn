---
sidebar_position: 1
---

# Classes

Classes em Scala são templates estáticos que podem ser instanciados como vários objetos em tempo de execução. Para construir uma classes combinamos:
  - Um construtor de paramêtros.
  - Métodos que são chamados no corpo da classe.
  - E campos contém estados.
  - Usar o operador *`new`*

```scala
// Construtor
class Politico(val nome: String, var crimes: Long) {

  // Campo
  val maiorMentira: String = "Vou arrumar o país"

  // Método
  def addCrime(crime: Int): Unit =
    this.crimes += crime
}

val p = new Politico("Meu politico favorito", 100)
```

## Métodos

Métodos fornecem operações e encapsulam operações já os modificadores de acesso declaram visibilidade ou ocultam informação.

```scala
// Construtor
class Politico(val nome: String, var crimes: Long) {

  // Campo
  val maiorMentira: String = "Vou arrumar o país"

  // Método
  def addCrime(crime: Int): Unit =
    this.crime += 1

  // Método
  def discurso(minutos: Int): Unit =
    for(i <- 1 to 2 * minutos) {
      println(this.maiorMentira)
    }
}

val p = new Politico("Meu politico favorito", 100)
p.discurso(2)
// Vou arrumar o país
// Vou arrumar o país
// Vou arrumar o país
// Vou arrumar o país
```

## Access e Modifiers

Classes criadas com var ou val geram *getters* ou *setters* como na tabela abaixo:

| Visibilidade | Acesso | Modificador|
| --- | --- | --- |
| *`var`* | sim | sim |
| *`val`* | sim | não |
| sem  *`var`* ou *`val`* | não | não |
| add *`private`* em *`var`* ou *`val`* | não | não |

Por padrão todos os membros são públicos, a palavra reservada *`private`* pode ser usada para restringir o acesso de fora da classe ou de alguma entidade específica usando *`private[MeuPacote]`*. Usando `protected` o membro se torna visível dentro das subclasses e entidades que o envolvem.

```scala
// Construtor
class Politico(val nome: String, var crimes: Long) {

  // Campo
  private val maiorMentira: String = "Vou arrumar o país"

  // Método
  def addCrime(crime: Int): Unit =
    this.crime += 1

  // Método
  def discurso(minutos: Int): Unit =
    for(i <- 1 to 2 * minutos) {
      println(this.maiorMentira)
    }

  // Método
  private[this] def mostrar(nome: String): Boolean = {
    println(s"${this.nome} e $nome são iguaizinhos")
    true
  }

  // Método
  def igual(politico: Any): Boolean =
    val valor = this.mostrar(politico.nome)
    politico match {
      case politico: Politico if valor == true =>  true
      case _ => false
    }
}

val b17 = new Politico("Bolsonaro", 10)
val l13 = new Politico("Lula", 9)

b17.igual(l13)
// Bolsonaro e Lula são iguaizinhos
// true
```

## Construtores e Valores Padrão

Podemos criar uma classe com valores padrão.

```scala
class Pizza(
    var tamanho: Int=4, var borda: String="catupiri"
  ) {

  override def toString = {
    if (tamanho <= 10)
      s"Pizza Pequena com borda de $borda"
    else
      s"Pizza Grande com borda $borda"
    }
}

val p = new Pizza
p.toString
// Pizza Pequena com borda de catupiri
```

Além disso também é possivel criar a classe com mais de um tipo de construtor padrão, nesse exemplo existe um *companion objects* que será explicado no próximos capítulo.
```scala
object Trem {
val name = "Expresso Brasil"
var local = 10
}

class Trem(val name: String, var estacao: Int) {

def this(name: String) = this(name, Trem.local)

def this(estacao: Int) = this(Trem.name, estacao)

def this() = this(Trem.name, Trem.local)

override def toString =
  s"Trem $name indo para estação $estacao"
}

val t = new Trem
p.toString
// Trem Expresso Brasil indo para estação 10"


val t = new Trem("Expresso Bahia")
p.toString
// Trem Expresso Bahia indo para estação 10"


val t = new Trem(15)
p.toString
// Trem Expresso Brasil indo para estação 15"


val t = new Trem("Expresso Minas", 12)
p.toString
// Trem Expresso Minas indo para estação 12"
```
