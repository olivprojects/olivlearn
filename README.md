# Website

 [![pipeline status](https://gitlab.com/olivprojects/olivlearn/badges/main/pipeline.svg)](https://gitlab.com/olivprojects/olivlearn/-/commits/main)


Esse site foi feito usando [Docusaurus 2](https://docusaurus.io/), que é um moderno gerador de páginas web estáticas.

## Pre-Requisitos

[Node](https://nodejs.org/en/download/) >= v12.13.0

[Yarn](https://classic.yarnpkg.com/en/) >= v1.5


## Instalação

```console
yarn install
```

## Rodando Local

Isso vai iniciar um servidor local e vai abrir uma janela no seu browser.

```console
yarn start
```